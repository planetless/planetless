#include <event_loop.hpp>

void event_loop(sf::RenderWindow* window)
{
    while (window->isOpen())
    {
        sf::Event Event;
        while (window->pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed) window->close();
        }
    }
}