#include <animation.hpp>
#include <iostream>
#include <render_loop.hpp>

void render_loop(sf::RenderWindow* window)
{
    window->setActive(true);

    sf::Texture texture;
    if (!texture.loadFromFile("assets/textures/earth.jpg"))
    {
        std::cerr << "cannot load assets/textures/earth.jpg" << std::endl;
    }

    while (window->isOpen())
    {
        sf::Sprite sprite(texture);
        window->clear();
        window->draw(sprite);
        window->display();
    }
}