#include <SFML/Graphics.hpp>
#include <event_loop.hpp>
#include <iostream>
#include <render_loop.hpp>

#ifdef __linux__
#include <X11/Xlib.h>
#endif

using namespace std;

int main(int argc, char* argv[])
{
#ifdef __linux__
    XInitThreads();
#endif

    sf::RenderWindow window(sf::VideoMode(800, 600), "planetless");
    window.setFramerateLimit(60);
    window.setActive(false);

    sf::Thread thread(&render_loop, &window);
    thread.launch();

    event_loop(&window);

    return 0;
}