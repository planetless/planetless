#include <iostream>

struct Stats
{
    Stats(int pic = 0, int coeur = 0, int trefle = 0) : pic(pic), coeur(coeur), trefle(trefle) {}

    int pic;
    int coeur;
    int trefle;
};

void print_stats(Stats stats)
{
    std::cout << "pic: " << stats.pic << std::endl;
    std::cout << "coeur: " << stats.coeur << std::endl;
    std::cout << "trefle: " << stats.trefle << std::endl;
    std::cout << std::endl;
}

void compute_something(Stats stats1, Stats stats2) {}

int main(int argc, char** argv)
{
    Stats main;
    Stats stats1;
    Stats stats2;

    compute_something(stats1, stats2);

    print_stats(main);
}