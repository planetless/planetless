#ifndef RENDER_LOOP_H
#define RENDER_LOOP_H

#include <SFML/Graphics.hpp>

void render_loop(sf::RenderWindow* window);

#endif