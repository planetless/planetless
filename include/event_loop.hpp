#ifndef EVENT_LOOP_H
#define EVENT_LOOP_H

#include <SFML/Graphics.hpp>

void event_loop(sf::RenderWindow* window);

#endif